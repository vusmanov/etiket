<?php include("includes/session.php"); ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Description" content="Тикет система E-TikeT">
	<meta name="Keywords" content="тикет, сообщение, поддержка">
	<title>Тикет система</title>
	<?php
	$_SESSION['auth'] = md5(uniqid(microtime()));
	//тест 3333333333333333333333333
	$filename = 'e_config.php';
	if (!file_exists($filename)) {
		define('css', 'css/bootstrap.min.css');
		echo "<p></p><strong>Внимание:</strong> Не найден файл e_config.php!</p>";
		echo "<p>Переименуйте скрипт <strong>e_config_del.php в e_config.php</strong></p>";
		echo "<p>Откройте скрипт e_config.php в текстовом редакторе <strong>и сделайте необходимые настройки</strong>.</p>";
		include("includes/footer.php");
		exit;
	}

	include("e_config.php");
	include("includes/header.php");

	//check database settings.
	include("includes/ez_sql_core.php");
	include("includes/ez_sql_mysqli.php");
	$db = new ezSQL_mysqli(db_user, db_password, db_name, db_host);
	$SCHEMA_NAME = $db->get_var("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" . db_name . "';");
	if ($SCHEMA_NAME <> db_name) {
		echo "<p></p><strong>Внимание:</strong> БД не найдена</p>";
		echo "<p>Укажите настройки БД (".db_name.") в файле e_config.php.</p>";
		include("includes/footer.php");
		exit;
	}

	//check if tables actually exist.
	$user_table_exists = $db->get_var("SHOW TABLES LIKE 'site_users';");
	if ($user_table_exists <>  "site_users") {
		echo "<p></p><strong>Внимание:</strong> таблици не найдены</p>";
		echo "<p>Импортируйте в (БД: " . db_name . ") файл <strong>bd.sql</strong></p>";
		include("includes/footer.php");
		exit;
	}

	if (isset($_SESSION['user_id'])) {
		$user_id = $_SESSION['user_id'];
		include("includes/all-nav.php");
		echo "<p>Добро пожаловать</p>";
		echo "<p><a href='e_dashboard.php'>Панель управления E-TikeT</a></p>";
	} else {
	?>

		<?php
		//limit login tries.
		if (isset($_SESSION['hit'])) {
			if ($_SESSION['hit'] > LOGIN_TRIES) {
				echo "<p><i class='fa fa-lock fa-2x pull-left'></i> Вы заблокированы!</p>";
				include("includes/footer.php");
				exit;
			}
		}
		?>

		<h2>Тикет система <?php echo E_TITLE; ?></h2>

		<?php
		if (isset($_GET['loggedout'])) {
			echo "<div class=\"alert alert-success\" style=\"max-width: 350px; text-align: center;\"><strong>Вы вышли</strong><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>";
		}
		?>

		<?php
		if (ALLOW_ANY_ADD == 'yes') {
			echo "<h4><a href='e_any_call_add.php' class='btn btn-success'>Открыть тикет <i class='glyphicon glyphicon-new-window'></i></a></h4>";
			echo "<hr>";
			echo "<p>или войти в тикет систему</p>";
		}
		?>

		<form action="e_login.php" method="post" class="form-horizontal" role="form">

			<div class="form-group">
				<label class="col-sm-2 control-label" for="inputEmail">Логин или Email</label>
				<div class="col-sm-3">
					<input type="text" id="inputEmail" name="user_login" placeholder="Логин / Email" required>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label" for="inputPassword">Пароль</label>
				<div class="col-sm-3">
					<input type="password" id="inputPassword" name="user_password" placeholder="Пароль" required>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary">Войти</button>
				</div>
			</div>
		</form>

		<p><?php if (ALLOW_REGISTER == "yes") { ?>
				<a href="e_register.php" class="btn btn-default">Регистрация</a>
			<?php } ?> <a href="e_forgotpassword.php" class="btn btn-default">Востановить пароль</a></p>
	<?php } ?>

	<?php include("includes/footer.php"); ?>