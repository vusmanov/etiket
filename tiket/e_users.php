<?php
include("includes/session.php");
include("includes/checksession.php");
?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Description" content="Тикет система E-TikeT">
	<meta name="Keywords" content="тикет, сообщение, поддержка">
	<title>Пользаватели</title>
	<?php
	include("e_config.php");
	include("includes/header.php");
	include("includes/all-nav.php");
	include("includes/ez_sql_core.php");
	include("includes/ez_sql_mysqli.php");
	include("includes/functions.php");
	$db = new ezSQL_mysqli(db_user, db_password, db_name, db_host);

	$pending = "";
	$title	 = "";

	if (isset($_GET['pending'])) {
		$pending = "AND user_pending = 1";
	}

	if (isset($_GET['support_staff'])) {
		$pending = "AND user_level = 2";
		$title = "Членов поддержки";
	}

	$myquery = "SELECT user_id,user_name,user_email,user_pending,user_level,user_protect_edit,user_msg_send from site_users where 1 $pending order by user_level,user_id desc;";
	$site_calls = $db->get_results($myquery);
	$num = $db->num_rows;
	echo "<p><a href='e_settings.php'>Настройки</a></p>";
	echo "<h4>$num $title</h4>";
	if ($num > 0) {
	?>

		<table class="<?php echo $table_style_2; ?>" style='width: auto;'>
			<tr>
				<th>ID</th>
				<th>Отк. тикетов</th>
				<th>Имя</th>
				<th>Email</th>
				<th>Уровень</th>
				<th>Дублировать на Email</th>
				<th>В ожидании</th>
				<th>Заблокирован</th>
			</tr>
		<?php
		foreach ($site_calls as $call) {
			$user_id = $call->user_id;
			$user_name = $call->user_name;
			$user_email  = $call->user_email;
			$user_pending = $call->user_pending;
			$user_protect_edit = $call->user_protect_edit;
			$user_level = $call->user_level;
			$user_msg_send = $call->user_msg_send;
			$bg = ($user_pending == 1) ? " class='usernote'" : "";
			$call_count = $db->get_var("SELECT count(call_id) from site_calls WHERE (call_user = $user_id) AND (call_status = 0);");
			echo "<tr>\n";
			echo "<td" . $bg . "><a href='e_edit_user.php?url_user_id=$user_id'>$user_id</a></td>\n";
			echo "<td align='center'><a href='e_calls.php?user_id=$user_id'>$call_count</a></td>\n";
			echo "<td>$user_name</td>\n";
			echo "<td>$user_email</td>\n";
			echo "<td>" . show_user_level($user_level) . "</td>\n";
			echo "<td style='text-align: center;'>" . onoff($user_msg_send) . "</td>\n";
			echo "<td style='text-align: center;'>" . onoff($user_pending) . "</td>\n";
			echo "<td style='text-align: center;'>" . onoff($user_protect_edit) . "</td>\n";
			echo "</tr>\n";
		}
	}
		?>
		</table>

		<?php
		include("includes/footer.php");
