<?php
include("includes/session.php");
include("includes/checksession.php");
include("includes/checksessionadmin.php");
?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Description" content="Тикет система E-TikeT">
	<meta name="Keywords" content="тикет, сообщение, поддержка">
	<title>Добавить пользователя</title>
	<?php
	include("e_config.php");
	include("includes/header.php");
	include("includes/all-nav.php");
	include("includes/functions.php");
	include("includes/ez_sql_core.php");
	include("includes/ez_sql_mysqli.php");
	$actionstatus = "";
	$db = new ezSQL_mysqli(db_user, db_password, db_name, db_host);
	//<ADD>
	if (isset($_POST['nacl'])) {
		if ($_POST['nacl'] == md5(AUTH_KEY . $db->get_var("select last_login from site_users where user_id = $user_id;"))) {
			//authentication verified, continue.
			$user_login = $db->escape($_POST['user_login']);
			$user_email = $db->escape($_POST['user_email']);
			//check email exists
			$num = $db->get_var("select count(user_email) from site_users where (user_email = '$user_email');");
			if ($num > 0) {
				echo "<div class='alert alert-danger'><strong>Ошибка:</strong> этот email уже используется</div>";
				include("includes/footer.php");
				exit;
			}

			//password function here
			if (strlen($_POST['user_password']) > 4) {
				$user_password = makepwd(trim($db->escape($_POST['user_password'])));
			} else {
				echo "<div class='alert alert-danger'><strong>Ошибка:</strong> password to short.</div>";
				include("includes/footer.php");
				exit;
			}

			$user_name = $db->escape($_POST['user_name']);
			$user_phone = $db->escape($_POST['user_phone']);
			$user_address = $db->escape($_POST['user_address']);
			$user_city = $db->escape($_POST['user_city']);
			$user_state = $db->escape($_POST['user_state']);
			$user_zip = $db->escape($_POST['user_zip']);
			$user_country = $db->escape($_POST['user_country']);
			$db->query("INSERT INTO site_users(user_login,user_email,user_password,user_name,user_phone,user_address,user_city,user_state,user_zip,user_country,user_level,user_status)VALUES('$user_login','$user_email','$user_password','$user_name','$user_phone','$user_address','$user_city','$user_state','$user_zip','$user_country',1,1);");
			//$db->debug();
			$actionstatus = "<div class=\"alert alert-success\" style=\"max-width: 250px;\">
    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    Пользователь добавлен.
    </div>";
		}
	}
	//</ADD>

	$nacl = md5(AUTH_KEY . $db->get_var("select last_login from site_users where user_id = $user_id;"));
	?>

	<h4>Добавить пользователя</h4>
	<?php echo $actionstatus; ?>

	<form action="e_admin_register.php" method="post" class="form-horizontal" data-parsley-validate>
		<table class="<?php echo $table_style_2; ?>" style='width: auto;'>
			<tr>
				<td>Логин*</td>
				<td><input type="text" name="user_login" size="20" required></td>
			</tr>

			<tr>
				<td>Email*</td>
				<td><input type="text" name="user_email" size="20" required data-parsley-type="email"></td>
			</tr>

			<tr>
				<td>Пароль*</td>
				<td><input type="text" name="user_password" size="20" required data-parsley-minlength="4"></td>
			</tr>

			<tr>
				<td>Имя*</td>
				<td><input type="text" name="user_name" size="20" required></td>
			</tr>

			<tr>
				<td>Телефон</td>
				<td><input type="text" name="user_phone" size="20"></td>
			</tr>

			<tr>
				<td>Адрес</td>
				<td><input type="text" name="user_address" size="20"></td>
			</tr>

			<tr>
				<td>Город</td>
				<td><input type="text" name="user_city" size="20"></td>
			</tr>

			<tr>
				<td>Область</td>
				<td><input type="text" name="user_state" size="20"></td>
			</tr>

			<tr>
				<td>Индекс</td>
				<td><input type="text" name="user_zip" size="20"></td>
			</tr>

			<tr>
				<td>Страна</td>
				<td><input type="text" name="user_country" size="20"></td>
			</tr>

		</table>
		<input type='hidden' name='nacl' value='<?php echo $nacl; ?>'>
		<input type="submit" value="Добавить" class="btn btn-primary">
	</form>

	<?php
	include("includes/footer.php");
