<?php $page = basename($_SERVER['REQUEST_URI']);
if (isset($_GET["recent"])) {
	$page = "recent";
	}
?>

<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="e_dashboard.php" title="dashboard"><?php echo E_TITLE;?></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      	<ul class="nav navbar-nav">
		<li<?php if($page == 'e_calls.php'){echo ' class="active"';};?>><a href="e_calls.php">Открытые тикеты</a></li>

		<?php
		switch ($_SESSION['user_level']) {
		    case 0:
		        $addpage = "e_call_add.php";
		        break;
		    case 1:
		        $addpage = "e_user_call_add.php";
				break;
		    case 2:
		        $addpage = "e_call_add.php";
				break;
		}
		$addpage = ($_SESSION['user_level'] == 1) ? "e_user_call_add.php" : "e_call_add.php"; ?>
		<li<?php if($page == $addpage){echo ' class="active"';};?>><a href="<?php echo $addpage;?>">Добавить тикет</a></li>

		<li<?php if($page == 'e_search.php'){echo ' class="active"';};?>><a href="e_search.php" title="Ticket Search">Поиск</a></li>
		<li<?php if($page == 'e_myaccount.php'){echo ' class="active"';};?>><a href="e_myaccount.php">Профиль</a></li>
		 <li><a href="includes/session.php?logout=y" title="Logout"><i class="fa fa-sign-out fa-lg"></i></a></li>

		<?php
		//ADMIN ONLY DROP DOWN NAV
		if(isset($_SESSION['admin'])){ ?>
		<li<?php if($page == 'recent'){echo ' class="active"';};?>><a href="e_search.php?call_status=&search=1&recent=1&call_date1=&call_date2=&call_email=&call_first_name=&call_phone=&call_department=&call_request=&call_device=&call_staff=&call_details=&call_solution=" title="Показать все тикеты">Все тикеты</a></li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Админ <b class="caret"></b></a>
          <ul class="dropdown-menu">
			<li<?php if($page == 'e_settings.php'){echo ' class="active"';};?>><a href="e_settings.php" title="Help Desk Settings">Настройки</a></li>
			<li<?php if($page == 'e_admin_register.php'){echo ' class="active"';};?>><a href="e_admin_register.php">Добавить пользователя</a></li>
			<li<?php if($page == 'e_users.php'){echo ' class="active"';};?>><a href="e_users.php">Изменить пользователя</a></li>
          </ul>
        </li>
		<?php } ?>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>