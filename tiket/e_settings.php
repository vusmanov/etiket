<?php
include("includes/session.php");
include("includes/checksession.php");
include("includes/checksessionadmin.php");
?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Description" content="Тикет система E-TikeT">
	<meta name="Keywords" content="тикет, сообщение, поддержка">
	<title>Настройка тикет системы</title>
	<?php
	include("e_config.php");
	include("includes/header.php");
	include("includes/all-nav.php");
	include("includes/functions.php");
	include("includes/ez_sql_core.php");
	include("includes/ez_sql_mysqli.php");
	$db = new ezSQL_mysqli(db_user, db_password, db_name, db_host);
	$encrypted_passwords = $db->get_var("SELECT option_value from site_options where option_name = 'encrypted_passwords';");
	$encrypted_link = "";
	if ($encrypted_passwords <> 'yes') {
		$encrypted_link = " <small><a href='e_admin_e.php'>Encrypt Passwords</a></small>";
	}
	$date = date_create();
	$fhddate = date_format($date, 'U')
	?>

	<h4>Настройка тикет системы</h4>

	<a href="e_settings_action.php?type=1" class="btn btn-default btn-sm"><i class="fa fa-cog"></i> Отделы</a>
	<a href="e_settings_action.php?type=2" class="btn btn-default btn-sm"><i class="fa fa-cog"></i> Важность</a>
	<a href="e_settings_action.php?type=3" class="btn btn-default btn-sm"><i class="fa fa-cog"></i> Категории</a>
	<a href="e_users.php?support_staff=show" class="btn btn-default btn-sm"><i class="fa fa-cog"></i> Персонал</a>

	<hr>
	<h4>Файл конфигурации:<small> e_config.php</small></h4>

	<table class="<?php echo $table_style_1; ?>" style='width: auto;'>
		<tr>
			<td>Попыток входа</td>
			<td><span class="label label-info"><?php echo LOGIN_TRIES; ?></span></td>
		</tr>

		<?php if (E_UPLOAD_ALLOW == "yes") {

			$upload_path = 'upload/test.txt';
			$write = "";
			if (!is_writable(dirname($upload_path))) {
				$write = " <span class='label label-danger'>[" . dirname($upload_path) . '] directory is not writable</span>';
			}
		?>

			<tr>
				<td>Разрешить загрузку</td>
				<td><?php echo yesno(E_UPLOAD_ALLOW) . $write; ?></td>
			</tr>
			<tr>
				<td>Разрешёные для загрузки типы файлов</td>
				<td>
					<?php foreach ($allowedExts as $v) {
						echo " <span class='label label-success'>$v</span>";
					}
					?>
				</td>
			</tr>
		<?php } ?>

		<tr>
			<td>Разрешить регистрацию</td>
			<td><?php echo yesno(ALLOW_REGISTER); ?></td>
		</tr>
		<tr>
			<td>Captcha при регистрации</td>
			<td><?php echo yesno(CAPTCHA_REGISTER); ?></td>
		</tr>
		<tr>
			<td>Captcha при востановлении пароля</td>
			<td><?php echo yesno(CAPTCHA_RESET_PASSWORD); ?></td>
		</tr>
		<tr>
			<td>Разрешить незарегистрированным пользователям открывать тикеты</td>
			<td><?php echo yesno(ALLOW_ANY_ADD); ?></td>
		</tr>
		<tr>
			<td>Настройки времени</td>
			<td><span class="label label-info"><?php echo E_TIMEADJUST; ?></span>
				<?php echo date('d.m.Y H:i:s'); ?> <i class="fa fa-arrow-circle-right"></i> <?php echo date('d.m.Y H:i:s', ($fhddate + (E_TIMEADJUST * 3600))); ?>
			</td>
		</tr>
		<tr>
			<td>Шифровать пароли</td>
			<td><?php echo yesno($encrypted_passwords); ?> <?php echo $encrypted_link; ?></td>
		</tr>
		<tr>
			<td>Email для уведомлений</td>
			<td><?php echo TO_EMAIL; ?></td>
		</tr>
		<tr>
			<td>От Email</td>
			<td><?php echo FROM_EMAIL; ?></td>
		</tr>
		<tr>
			<td>Имя базы данных</td>
			<td><?php echo db_name; ?></td>
		</tr>

		<tr>
			<td>Стиль CSS <a href="http://bootswatch.com/" target="_blank" class="btn btn-default btn-xs">Bootstrap</a></td>
			<td><?php echo css; ?></td>
		</tr>

	</table>

	<?php
	include("includes/footer.php");
