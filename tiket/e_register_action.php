<?php include("includes/session.php"); ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Description" content="Тикет система E-TikeT">
	<meta name="Keywords" content="тикет, сообщение, поддержка">
	<title>Регистрация</title>
	<?php
	include("e_config.php");
	include("includes/header.php");
	include("includes/ez_sql_core.php");
	include("includes/ez_sql_mysqli.php");
	include("includes/functions.php");
	//initilize db
	$db = new ezSQL_mysqli(db_user, db_password, db_name, db_host);

	if (ALLOW_REGISTER <> "yes") {
		echo "<p>Регистрация закрыта</p>";
		include("includes/footer.php");
		exit;
	}

	if (CAPTCHA_REGISTER == "yes") {
		$captchasession = $_SESSION['captcha']['code'];
		$captcha = $db->escape(trim($_POST['captcha']));
		if ($captchasession <> $captcha) {
			echo "<div class=\"alert alert-danger\" style=\"max-width: 350px;\">Неправильно введена капча.</div>";
			include("includes/footer.php");
			exit;
		}
	}

	//IP and DATE field
	$ip = $_SERVER['REMOTE_ADDR'];


	//EMAIL address
	$email = $db->escape(trim($_POST['email']));

	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		echo "<div class=\"alert alert-danger\" style=\"max-width: 350px;\">Email неправльный.</div>";
		include("includes/footer.php");
		exit;
	}

	if ($email) {
		//check if email already exists.
		$num = $db->get_var("select count(user_email) from site_users where user_email = '$email';");
		if ($num > 0) {
			echo "<div class=\"alert alert-danger\" style=\"max-width: 350px;\">Email уже зарегистрирован.</div>";
			include("includes/footer.php");
			exit;
		}
	} else {
		echo "<div class=\"alert alert-danger\" style=\"max-width: 350px;\">Веедите Email.</div>";
		include("includes/footer.php");
		exit;
	}

	//NAME FIELD
	$name = $db->escape(trim(strip_tags($_POST['name'])));
	$strlen = (strlen($name));
	if ($strlen < 3) {
		echo "<div class=\"alert alert-danger\" style=\"max-width: 350px;\">Имя должно быть не менее 3 символов.</div>";
		include("includes/footer.php");
		exit;
	}

	//LOGIN NAME FIELD
	$login = $db->escape(trim(strip_tags($_POST['login'])));
	//make sure search length is at least 15 chars.
	$strlen = (strlen($login));
	if ($strlen < 3) {
		echo "<div class=\"alert alert-danger\" style=\"max-width: 350px;\">Логин должен быть не менее 3 символов.</div>";
		include("includes/footer.php");
		exit;
	}
	//check if login name is unique.
	$num = $db->get_var("select count(user_login) from site_users where user_login = '$login';");
	if ($num > 0) {
		echo "<div class=\"alert alert-danger\" style=\"max-width: 350px;\">Такой логин уже есть.</div>";
		include("includes/footer.php");
		exit;
	}

	//PASSWORD FIELD
	$password = $db->escape(trim(strip_tags($_POST['password'])));
	if ($password) {
		$passwordlength = strlen($password);
		if ($passwordlength >= 5) {
			$user_password = makepwd(trim($db->escape($password)));
		} else {
			echo "<div class=\"alert alert-danger\" style=\"max-width: 350px;\">Пароль должен быть не менее 5 символов.</div>";
			include("includes/footer.php");
			exit;
		}
	}

	//pending
	if (REGISTER_APPROVAL == "yes") { //Все регистрации должны быть одобрены администратором yes/no 
		$user_pending = 1;
		echo "<div class=\"alert alert-danger\" style=\"max-width: 350px;\">Вы зарегистрированы, ожидайте активации.</div>";
	} else {
		$user_pending = 0;
	}

	//user_msg_send
	$user_msg_send = 1;

	$query = "INSERT INTO site_users(user_login,user_email,user_name,user_password,last_ip,user_status,user_level,user_pending,user_msg_send)VALUES('$login','$email','$name','$user_password','$ip',1,1,$user_pending,$user_msg_send);";
	$db->query($query);
	//notify admin
	$from	 = FROM_EMAIL;
	$to      = TO_EMAIL;
	$subject = E_TITLE . ' Новая регистрация';
	$dat = date("d.m.y H:i:s");
	// message
	$message = '
<html>
<head>
  <title>Новая регистрация</title>
</head>
<body>
  <p>Новая регистрация</p>
  <p>Дата / время: ' . $dat . '</p>
  <p>Имя: ' . $name . '</p>
  <p>Логин: ' . $login . '</p>
  <p>Email: ' .  $email . '</p>
</body>
</html>
';
	$headers = "From:" . $from . "\r\n";
	$headers .= "Reply-To: " . $from . "\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";
	$headers .= "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
	mail($to, $subject, $message, $headers);
	?>

	<h3>Вы зарегистрированы</h3>

	<h4><a href="index.php">Войти</a></h4>

	<?php include("includes/footer.php");
