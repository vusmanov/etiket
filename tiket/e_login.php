<?php include("includes/session.php"); ?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Description" content="Тикет система E-TikeT">
	<meta name="Keywords" content="тикет, сообщение, поддержка">
	<title>Служба поддержки</title>
	<?php
	$is_valid = 0;
	include("e_config.php");
	include("includes/header.php");
	include("includes/functions.php");

	if (!isset($_SESSION['auth'])) {
		echo "<p>Ошибка авторизации</p><p><i class='fa fa-lock'></i></p>";
		include("includes/footer.php");
		exit;
	}

	//limit login tries.
	if (isset($_SESSION['hit'])) {
		$_SESSION['hit'] += 1;
		if ($_SESSION['hit'] > LOGIN_TRIES) {
			echo "<p><i class='fa fa-lock fa-2x pull-left'></i> Доступ заблокирован</p>";
			include("includes/footer.php");
			exit;
		}
	} else {
		$_SESSION['hit'] = 0;
	}

	include("includes/ez_sql_core.php");
	include("includes/ez_sql_mysqli.php");
	$db = new ezSQL_mysqli(db_user, db_password, db_name, db_host);

	if (isset($_POST['user_login'])) {
		$user_login = trim($db->escape($_POST['user_login']));
	} else {
		echo "<div class='alert alert-warning' style='width: 375px;'><i class='glyphicon glyphicon-info-sign'></i> Логин / Emai.</div>";
		include("includes/footer.php");
		exit;
	}

	if (isset($_POST['user_password'])) {
		$user_password = trim($db->escape($_POST['user_password']));
		$is_valid = checkpwd($user_password, $user_login);
	}

	//uesrs can login with either login name or email address.
	$pos = strrpos($user_login, "@");
	if ($pos === false) { // note: three equal signs 
		$checkusing = "user_login";
	} else {
		$checkusing = "user_email";
	}

	$is_pending = $db->get_var("select user_pending from site_users where user_login = '$user_login' OR user_email = '$user_login' limit 1;");
	if ($is_pending == 1) {
		//если пользователь ожидает, то установите недействительным значение 0
		$is_valid = 0;
	}


	if ($is_valid <> 1) {
		$_SESSION['hit'] += 1;
		echo "<div class='alert alert-warning' style='width: 375px;'><i class='glyphicon glyphicon-info-sign'></i> Логин неверный, или ваша учётная запись не активированна.</div>";
		include("includes/footer.php");
		exit;
	}

	$site_users = $db->get_row("select user_id,user_name,user_level from site_users WHERE $checkusing = '$user_login' limit 1;");
	$user_id = $site_users->user_id;
	$user_name = $site_users->user_name;
	$user_level = $site_users->user_level;

	if ($user_level == 0) {
		$_SESSION['admin'] = 1;
	} else {
		$_SESSION['user'] = 1;
	}
	// Для авторизации через сторонние скрипты в тикет системе, передайте эти значения в сессии
	// Юзер должен быть в БД тикет системы!
	$_SESSION['user_id'] = $user_id; // Ид юзера
	$_SESSION['user_name'] = $user_name; // Имя юзера
	$_SESSION['user_level'] = $user_level; //уровень, админ = 0 юзер = 1
	$_SESSION['hit'] = 0; //обнуляем количество попыток входа

	include("includes/all-nav.php");

	echo "<!-- <p>$user_id</p> -->";
	echo "<h2>Добро пожаловать, $user_name</h2>";

	//запишите некоторые детали об этом логине
	$lastip = $_SERVER['REMOTE_ADDR'];
	$last_login = date(time());

	$db->query("UPDATE site_users SET last_ip = '$lastip',last_login = '$last_login' WHERE user_id = $user_id;");

	?>

	<h3><a href="e_user_call_add.php" class="btn btn-large btn-primary btn-success">Открытые тикеты</a></h3>

	<h3><a href="e_calls.php" class="btn btn-large btn-primary">Посмотреть тикеты</a></h3>

	<?php include("includes/footer.php");
