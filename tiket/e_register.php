<?php
session_start();
$_SESSION = array();
include("e_simple-php-captcha.php");
$_SESSION['captcha'] = simple_php_captcha();
?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Description" content="Тикет система E-TikeT">
	<meta name="Keywords" content="тикет, сообщение, поддержка">
	<title>Регистрация</title>

	<?php
	include("includes/ajax.php");
	include("e_config.php");
	include("includes/header.php");
	if (ALLOW_REGISTER <> "yes") {
		echo "<div class=\"alert alert-info\" style=\"width: 250px;\">Регистрация закрыта</div>";
		include("includes/footer.php");
		exit;
	}
	?>
	<h1><?php echo E_TITLE; ?> Регистрация</h1>
	<table class="<?php echo $table_style_2; ?>" style='width: auto;'>
		<form action="e_register_action.php" method="post" class="form-horizontal">
			<tr>
				<td>Ваше Имя:</td>
				<td><input type="text" name="name" id="name"></td>
			</tr>
			<tr>
				<td>Ваш логин:</td>
				<td><input type="text" name="login" onblur="showResult(this.value)" required> <span id="txtHint"></span></td>
			</tr>
			<tr>
				<td>email:</td>
				<td><input type="text" id="email" name="email" placeholder="name@example.com"></td>
			</tr>
			<tr>
				<td>Пароль:</td>
				<td><input type="password" id="password" name="password" placeholder="Минимум 5 символов"></td>
			</tr>
			<?php
			if (CAPTCHA_REGISTER == "yes") {
				$captchaimg = '<img src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA" />';
			?>
				<tr>
					<td><?php echo $captchaimg; ?></td>
					<td>Введите код:<br><input type="text" name="captcha" id="captcha" required></td>
				</tr>
			<?php } ?>
	</table>
	<br>
	<p><input type="submit" value="Регистрация" class="btn btn-primary btn-large"></p>
	<input type="hidden" name="try" value="true">
	</form>
	<!-- validation -->
	<script type="text/javascript" src="js/livevalidation_standalone.compressed.js"></script>
	<style type="text/css">
		.LV_valid {
			color: green;
			margin: 0 0 0 5px;
		}

		.LV_invalid {
			color: #CC0000;
			margin: 0 0 0 5px;
		}
	</style>
	<!-- validation -->
	<script type="text/javascript">
		var name = new LiveValidation('name', {
			wait: 500,
			validMessage: "Ок"
		});
		name.add(Validate.Presence, {
			failureMessage: " Обязательно"
		});
		name.add(Validate.Length, {
			minimum: 2
		});

		var email = new LiveValidation('email', {
			wait: 500,
			validMessage: "Ок"
		});
		email.add(Validate.Presence, {
			failureMessage: " Обязательно"
		});
		email.add(Validate.Email);

		var password = new LiveValidation('password', {
			wait: 500,
			validMessage: "Ок"
		});
		password.add(Validate.Presence, {
			failureMessage: " Обязательно"
		});
		password.add(Validate.Length, {
			minimum: 5
		});

		var captcha = new LiveValidation('captcha', {
			wait: 2000,
			validMessage: "Ок"
		});
		captcha.add(Validate.Presence, {
			failureMessage: " Обязательно"
		});
		captcha.add(Validate.Length, {
			minimum: 5
		});
	</script>

	<h4><i class="fa fa-arrow-left"></i> <a href="index.php">Назад</a></h4>

	<?php include("includes/footer.php");
