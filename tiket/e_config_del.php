<?php
define('db_name', 'helpd'); // Имя БД
define('db_user', 'root'); // Пользователь БД
define('db_password', ''); // Пароль
define('db_host', 'localhost'); // Расположение БД Обычно localhost

/** настроить отображение времени в часах */
define('E_TIMEADJUST', '+0'); //константа имя и значение

/** Установите AUTH KEY для безопасности.*/
define('AUTH_KEY', 'change this key'); //константа имя и значение

/** Установите, сколько попыток входа в систему (session only)*/
define('LOGIN_TRIES', 10); //константа имя и значение

/** email для отправки уведомлений FROM: */
define('FROM_EMAIL', 'postmaster@example.com'); //константа имя и значение

/** email для отправки уведомлений TO:  */
define('TO_EMAIL', 'postmaster@example.com'); //константа имя и значение

/** Разрешить регистрации yes/no */
define('ALLOW_REGISTER', 'yes'); //константа имя и значение

/** Use CAPTCHA with registration? yes/no */
define('CAPTCHA_REGISTER', 'no');

/** Use CAPTCHA with the forgot password form? yes/no */
define('CAPTCHA_RESET_PASSWORD', 'no');

/** Все регистрации должны быть одобрены администратором yes/no */
define('REGISTER_APPROVAL', 'no');

/** Разрешить незарегистрированным пользователям отправлять запросы yes/no  */
define('ALLOW_ANY_ADD', 'no');

/** Название организации **/
define('E_TITLE', "E-TikeT");

/** Разрешить загрузку ** yes or no */
define('E_UPLOAD_ALLOW', "yes");
define('UPLOAD_KEY', 'lkj789vh)');
//SET WHAT FILE EXTENSIONS ARE ALLOWED TO BE UPLOADED (comma seperated list "txt","pdf")
$allowedExts = array("jpg", "jpeg", "gif", "png", "doc", "docx", "wpd", "xls", "xlsx", "pdf", "txt", "pps", "pptx", "pub");

/** Темы (bootswatch.com), раскоментируйте для применения **/
//Раскомментируйте только одну строку (удалите // чтобы установить тему)
//define('css', 'css/bootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/amelia/bootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/cerulean/bootstrap.min.css');
define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/cosmo/bootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/cyborg/bootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/flatly/bootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/journal/bootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/lumen/bootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/readable/bootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/lumen/bootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/simplexbootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/slate/bootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/spacelab/bootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/superhero/bootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/united/bootstrap.min.css');
//define('css', '//netdna.bootstrapcdn.com/bootswatch/3.1.1/yeti/bootstrap.min.css');
